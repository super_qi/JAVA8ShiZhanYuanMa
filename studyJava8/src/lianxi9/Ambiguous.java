package lianxi9;

public class Ambiguous {

	
	public static void main(String[] args) {
		new C().Hello();
	}
	
	static interface A{
		public default void Hello(){
			System.out.println("Hello from A");
		}
	}
	
	static interface B{
		public default void Hello(){
			System.out.println("Hello from B");
		}
	}
	
	static class C implements B,A{

		public void Hello() {
			A.super.Hello();
		}
	}
}
