package lianxi9;

public class Diamond {

	public static void main(String[] args) {
		new D().Hello();
	}
	
	static interface A{
		public default void Hello(){
			System.out.println("Hello from A");
		}
	}
	
	static interface B extends A{}
	
	static interface C extends A{}
	
	static class D implements B,C{}
	
	
	
	
}
