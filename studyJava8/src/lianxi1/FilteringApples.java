package lianxi1;

import java.util.*;
import java.util.function.Predicate;

import bean.Apple;

public class FilteringApples {

	public static void main(String[] args) {
		
		List<Apple>  inventory=Arrays.asList(new Apple(80,"green"),
											 new Apple(155,"green"),
											 new Apple(120,"red"));
		
		List<Apple>  greenApples=filterApples(inventory, FilteringApples::isGreenApple);
	
	    List<Apple>  heavyAppkes=filterApples(inventory, FilteringApples::isHeavyApple);
		
	    List<Apple>  greenApples2=filterApples(inventory, (Apple a)-> "green".equals(a.getColor()) );
	    
	    List<Apple>  heavyApples2=filterApples(inventory, (Apple a ) -> a.getWeight()>150 );
	    
	    
	
	}
	
	
	
	

	/**
	 * 判断颜色是否是需要的绿色
	 * @param apple
	 * @return
	 */
	public static boolean isGreenApple(Apple apple){
		
		return "green".equals(apple.getColor());
	}
	
	/**
	 * 判断其重量是否大于150克
	 * @param apple
	 * @return
	 */
	public static boolean isHeavyApple(Apple apple){
		return apple.getWeight()>150;
	}


	/**
	 * 根据不同属性去找苹果集合
	 * @param inventory
	 * @param p
	 * @return
	 */
	public static List<Apple> filterApples(List<Apple> inventory,Predicate<Apple> p){
		
		List<Apple> result=new ArrayList<Apple>();
		for(Apple apple: inventory){
			if(p.test(apple)){
				result.add(apple);
			}
		}
		
		return result;
	}
	
	
	public static  class Apple {
	    private int weight = 0;
	    private String color = "";

	    public Apple(int weight, String color){
	        this.weight = weight;
	        this.color = color;
	    }

	    public Integer getWeight() {
	        return weight;
	    }

	    public void setWeight(Integer weight) {
	        this.weight = weight;
	    }

	    public String getColor() {
	        return color;
	    }

	    public void setColor(String color) {
	        this.color = color;
	    }

	    public String toString() {
	        return "Apple{" +
	               "color='" + color + '\'' +
	               ", weight=" + weight +
	               '}';
	    }
	}
	
	
}
