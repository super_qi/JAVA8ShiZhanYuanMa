package lianxi5;

import static lianxi4.Dish.menu;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import lianxi4.Dish;
public class Reducing {

	public static void main(String[] args) {
		List<Integer> numbers=Arrays.asList(3,4,5,1,2);
		int num =numbers.stream().reduce(0, (a,b)->a+b);
		
		System.out.println(num);
		
		int num2=numbers.stream().reduce(0, Integer::sum);
		
		System.out.println(num2);
		
		int max =numbers.stream().reduce(0,(a,b)-> Integer.max(a, b));
		System.out.println(max);
		
		Optional<Integer> min =numbers.stream().reduce(Integer::min);
		min.ifPresent(System.out::println);
		
		int calories =menu.stream()
						  .map(Dish::getCalories)
						  .reduce(0, Integer::sum);
		System.out.println("Number of calories: "+calories);
		
	}

	
	
}
