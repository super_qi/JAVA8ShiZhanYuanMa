package lianxi5;

import static  lianxi4.Dish.menu;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;

import lianxi4.Dish;
public class Mapping {

	
	public static void main(String[] args) {
		List<String> dishNames=
				menu.stream()
					.map(Dish::getName)
					.collect(toList());
		System.out.println(dishNames);

		System.out.println("-----------------");
		
		List<String> words=Arrays.asList("Hello","World");
		
		List<Integer> wordsLengths=
						words.stream()
						.map(String::length)
						.collect(toList());
		System.out.println(wordsLengths);
		
		System.out.println("-----------------");
		words.stream().flatMap((String line) -> Arrays.stream(line.split("")))
					  .distinct()
					  .forEach(System.out::println);

		System.out.println("-------------");
		
		List<Integer> number1 =Arrays.asList(1,2,3,4,5);
		List<Integer> number2 =Arrays.asList(6,7,8);
		List<int[]> pairs=
					number1.stream()
						   .flatMap((Integer i)-> number2.stream()
								   					.map((Integer j)-> new int[]{i, j})
								   	)
						   .filter(pair -> (pair[0]+pair[1])%3 ==0)
						   .collect(toList());
		pairs.forEach(pair -> System.out.println("("+pair[0]+","+pair[1]+")"));
						
		
			
		
	}
}
