package lianxi5;

import static lianxi4.Dish.menu;

import java.util.*;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import java.util.stream.*;

import lianxi4.Dish;
public class NumericStreams {

	public static void main(String[] args) {
		
		List<Integer> numbers =Arrays.asList(3,4,5,1,2);
		
		Arrays.stream(numbers.toArray()).forEach(System.out::println);
		
		System.out.println("---------------------");
		int calories =menu.stream()
						  .mapToInt(Dish::getCalories)
						  .sum();
		System.out.println("Number of caloriies" + calories);
	
		OptionalInt maxCalories =menu.stream()
									 .mapToInt(Dish::getCalories)
									 .max();
		int max;
		if(maxCalories.isPresent()) {   //在包含值得时候 返回true 
			max =maxCalories.getAsInt();
		}
		else{
			max=1;
		}
		System.out.println(max);

		IntStream evenNubers=IntStream.rangeClosed(1, 100)
									  .filter(n->n%2 ==0);
		System.out.println(evenNubers.count());
		System.out.println("--------------------");
		
		//boxed 将数值流转化为Stream
		Stream<int[]> pythagoreanTriples=
								IntStream.rangeClosed(1,100).boxed()
								.flatMap(a->IntStream.rangeClosed(a, 100)
													.filter(b -> Math.sqrt(a*a+b*b)%1==0).boxed()
													.map(b->new int[]{a,b,(int)Math.sqrt(a*a+b*b)}
													)
										);
		pythagoreanTriples.forEach(t->System.out.println(t[0]+","+t[1]+","+t[2]));						
								
	}
	
	public static boolean isPerfectSquare(int n){ 
		//sqrt  返回一个正平方根的值  如果n为9  返回3.0  
		return Math.sqrt(n)%1==0;
	}
}
