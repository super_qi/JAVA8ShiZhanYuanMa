package lianxi5;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.Comparator.comparing;
public class PutingIntoPratice {

	
	public static void main(String[] args) {
	
		Trader raoul =new Trader("Raoul","Cambridge");
		Trader mario =new Trader("Mario","Milan");
		Trader alan =new Trader("Alan","Cambridge");
		Trader brain =new Trader("Brian","Cambridge");
		
		List<Transaction> transactions=Arrays.asList(
				new Transaction(brain, 2011, 300),
				new Transaction(raoul, 2012, 1000),
				new Transaction(raoul, 2011, 300),
				new Transaction(mario, 2012, 710),
				new Transaction(mario, 2012, 700),
				new Transaction(alan, 2012, 950)
		);
		
		
		List<Transaction> tr2011=transactions.stream()
											 .filter(transaction -> transaction.getYear() ==2011)
											 .sorted(comparing(Transaction::getValue))
											 .collect(toList());
		System.out.println(tr2011);
		System.out.println("----------------");
		
		List<String> cities =transactions.stream()
										 .map(transaction->transaction.getTrader().getCity())
										 .distinct()
										 .collect(toList());
		System.out.println(cities);
		System.out.println("----------------");
		List<Trader> traders= transactions.stream()
								.map(Transaction::getTrader)
								.filter(trader -> trader.getCity().equals("Cambridge"))
								.distinct()
								.sorted(comparing(Trader::getName))
								.collect(toList());
		System.out.println(traders);
		System.out.println("----------------");
		String traderStr=
					transactions.stream()
								.map(transaction -> transaction.getTrader().getName())
								.distinct()
								.sorted()
								.reduce("", (n1,n2)-> n1+n2);
		System.out.println(traderStr);
		System.out.println("----------------");
		boolean milanBased=
						transactions.stream()
						.anyMatch(transaction-> transaction.getTrader()
														   .getCity()
														   .equals("Milan"));
		System.out.println(milanBased);
		System.out.println("----------------");
		transactions.stream()
					.map(Transaction::getTrader)
					.filter(trader -> trader.getCity().equals("Milan"))
					.forEach(trader->trader.setCity("Cambridge"));
		System.out.println(transactions);
		System.out.println("----------------");					
		int hightestValue=
				transactions.stream()
							.map(Transaction::getValue)
							.reduce(0,Integer::max);
		System.out.println(hightestValue);
	}
}
