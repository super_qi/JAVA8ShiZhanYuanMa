package lianxi4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.*;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class StreamBasic {

	
	
	public static void main(String[] args) {
		getLowCaloricDishesNamesInjava7(Dish.menu).forEach(System.out::println);
		
		System.out.println("---");

		getLowCaloricDishesNamesInJava8(Dish.menu).forEach(System.out::println);
	}
	
	
	public static List<String> getLowCaloricDishesNamesInjava7(List<Dish> disher){
		List<Dish> lowCaloricDishes=new ArrayList<>();
		for(Dish d:disher){
			if(d.getCalories() <400){
				lowCaloricDishes.add(d);
			}
		}
		
		List<String> lowCaloricDishesName=new ArrayList<>();
		Collections.sort(lowCaloricDishes,new Comparator<Dish>() {

			@Override
			public int compare(Dish o1, Dish o2) {
				return Integer.compare(o1.getCalories(), o2.getCalories());
			}
		});
		for(Dish d:lowCaloricDishes){
			lowCaloricDishesName.add(d.getName());
		}
		return lowCaloricDishesName;
	}
	
	public static List<String> getLowCaloricDishesNamesInJava8(List<Dish> dishes){
		return dishes.stream().filter(d->d.getCalories()<400)
				.sorted(comparing(Dish::getCalories)).
				map(Dish::getName).collect(toList());
		
	}
}
