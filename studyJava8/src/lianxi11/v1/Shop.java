package lianxi11.v1;

import  static lianxi11.Util.delay;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
public class Shop {
	
	/**
	 * final 变量 因为是不可变的  需要刚开始初始化
	 */
	private final String name ;
	
	private final Random random ;

	public String getName() {
		return name;
	}

	public Random getRandom() {
		return random;
	}

	public Shop(String name) {
		this.name = name;
		this.random = new Random(name.charAt(0)* name.charAt(1)*name.charAt(2));
	}
	
	
	public double getPrice (String product){
			return calculatePrice(product)	;
	}
	
	private double calculatePrice(String product){
		delay();
		return random.nextDouble()*product.charAt(0)+product.charAt(1);
	}
	
	public Future<Double> getPriceAsync(String product){
		CompletableFuture<Double> futurePrice =new CompletableFuture<>();
		new Thread(()->{
			double price =calculatePrice(product);
			futurePrice.complete(price);
		}).start();
		
		return futurePrice;
	}
	

	
	
}
