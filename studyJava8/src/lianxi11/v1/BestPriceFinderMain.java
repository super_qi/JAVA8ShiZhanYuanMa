package lianxi11.v1;

import java.util.List;
import java.util.function.Supplier;

public class BestPriceFinderMain {

	private static BestPriceFinder  bestPriceFinder =new BestPriceFinder();
	
	public static void main(String[] args) {
		execute("sequential", ()->bestPriceFinder.findPricessSequential("myPhone27s") );
		execute("paraller",() -> bestPriceFinder.findPricesParallel("myPhone27s") );
		execute("composed CompletableFuture",()->bestPriceFinder.findPricesFuture("myPhone27s"));
		execute("combined USD CompletableFuture", ()-> bestPriceFinder.findPricesInUSD("myPhone27s"));
		execute("combined USD CompletableFuture V2",() -> bestPriceFinder.findPriceInUSD2("myPhone27s") );
		execute("combined USD CompletableFuture v3", () -> bestPriceFinder.findPricesInUSD3("myPhone27s"));
	
	}
	
	private static void execute(String msg,Supplier<List<String>> s){
		long start =System.nanoTime();
		System.out.println(s.get());
		long duration =(System.nanoTime() -start)/ 1_000_000;
		System.out.println(msg+" done in "+ duration + " msecs");
	}
	
}
