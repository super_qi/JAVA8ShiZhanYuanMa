package lianxi2;

public class FileringApples {

	
	
	
	
	
    interface ApplePredicate{
    	public boolean test(Apple a);
    }
	
	
	public static class Apple {
	    private int weight = 0;
	    private String color = "";

	    public Apple(int weight, String color){
	        this.weight = weight;
	        this.color = color;
	    }

	    public Integer getWeight() {
	        return weight;
	    }

	    public void setWeight(Integer weight) {
	        this.weight = weight;
	    }

	    public String getColor() {
	        return color;
	    }

	    public void setColor(String color) {
	        this.color = color;
	    }

	    public String toString() {
	        return "Apple{" +
	               "color='" + color + '\'' +
	               ", weight=" + weight +
	               '}';
	    }
	}
	
	static class AppleWeightPredicate implements ApplePredicate{

		@Override
		public boolean test(Apple a) {
			return a.getWeight()>150;
		}
		
	}
	
	static class AppleColorPredicate implements ApplePredicate{

		@Override
		public boolean test(Apple a) {
			return "green".equals(a.getColor());
		}
		
	}
	
	static class AppleRedAndHeavyPredicate implements ApplePredicate{

		@Override
		public boolean test(Apple a) {
			return "red".equals(a.getColor()) && a.getWeight()>150;
		}
		
	}
}
