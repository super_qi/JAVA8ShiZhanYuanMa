package lianxi10;


import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Optional;
import java.util.Properties;
public class ReadPositiveIntOaram {

	
	public static int readDurationImperative(Properties pros,String name){
		
		String value =pros.getProperty(name);
		if(value != null){
			try {
				int i =Integer.parseInt(value);
				if(i>0){
					return i;
				}
			} catch (Exception e) {
			}
		}
		return 0;
	}
	
	public static int readDurationWithOptional(Properties props,String name){
		return Optional.ofNullable(props.getProperty(name))
					   .flatMap(ReadPositiveIntOaram :: s2i)
					   .filter(i -> i >0).orElse(0);
	}
	
	
	
	public static Optional<Integer> s2i(String s){
		
		try {
			return of(Integer.parseInt(s));
		} catch (Exception e) {
			return empty();
		}
	}
}
