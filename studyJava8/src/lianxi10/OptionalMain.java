package lianxi10;

import java.util.*;
import java.util.Optional;

import static java.util.stream.Collectors.toSet;
public class OptionalMain {

	public String getCarInsuranceName(Optional<Person> person){
		return person.flatMap(Person::getCar)
					 .flatMap(Car::getInsurance)
					 .map(Insurance::getName)
					 .orElse("Unknown");
	}
	
	
	//无法编译 。。。。
//	public Set<String> getCarInsuranceNames(List<Person> person){
//		return person.stream().map(Person::getCar)
//							  .map(optCat -> optCat.flatMap(Car::getInsurance))
//							  .map(optInsurance-> optInsurance.map(Insurance::getName))
//							  .flatMap(Optional::stream) 
//							  .collect(toSet());
//	}
}
