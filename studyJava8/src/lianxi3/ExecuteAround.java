package lianxi3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOError;
import java.io.IOException;

public class ExecuteAround {

	
	
	public static void main(String[] args) {
	
		String result;
		try {
			result = processFileLimited();
			System.out.println(result);
		
			String oneLine =processFile((BufferedReader b)->b.readLine());
			
			String twoLine =processFile((BufferedReader b)->b.readLine()+b.readLine());
			System.out.println(twoLine);

		
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static String processFileLimited() throws Exception{
		try(BufferedReader  br= new BufferedReader(
				new FileReader("lamdbasinaction/chap3/data.txt"))){
			return br.readLine();
		}
			
	}
	
	public static String processFile(BufferedReaderProcessor p) throws Exception{
		try(BufferedReader  br= new BufferedReader(
				new FileReader("lamdbasinaction/chap3/data.txt"))){
			return p.process(br);
		}
			
	}
	public  interface BufferedReaderProcessor{
		public String process(BufferedReader b) throws IOException;
			
	}
}
