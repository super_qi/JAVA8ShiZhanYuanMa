package lianxi8;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class FactoryMain {

	public static void main(String[] args) {
		Product p1=ProductFactory.createProduct("loan");
		System.out.println(p1.toString());
		
		Supplier<Product> loanSupplier= Loan::new;
		
		Product p2=loanSupplier.get();
		System.out.println(p2.toString());
		
		Product p3= ProductFactory.createProductLamdba("loan");
		System.out.println(p3.toString());
		
	}
	
	
	static private class ProductFactory{
		
		public static Product createProduct(String name){
			switch (name) {
			case "loan": return new Loan();
			case "stock" : return new Stock();
			case "bond": return new Bond();
			default: throw new RuntimeException("No such product "+ name) ;
			}
		}
		
		public static Product createProductLamdba(String name){
			Supplier<Product> p = map.get(name);
			if(p != null){
				return p.get();
			}
			throw new RuntimeException("No such product "+name);
		}
	}
	
	static private interface Product{}
	
	static private  class Loan implements Product{}
	
	static private class Stock implements Product{}
	
	static private class Bond implements Product{}
	
	final static private Map<String, Supplier<Product>> map =new HashMap();
	
	
	static{
		map.put("loan", Loan::new );
		map.put("stock", Stock::new);
		map.put("bond", Bond::new);
	}
	
}
