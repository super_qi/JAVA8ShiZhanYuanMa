package test;

import java.util.concurrent.TimeUnit;

public class Test {

	
	public static void main(String[] args) throws InterruptedException {
		Thread t1=new Thread(){
			@Override
			public void run() {
				while(true){
					
					if(Thread.currentThread().isInterrupted()){
						System.out.println("中断了");
						break;
						
					}
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						System.out.println("当睡眠的时候 中断");
						Thread.currentThread().interrupt();
					}
					Thread.yield();
				}
			}
		};
		t1.start();
		Thread.sleep(2000);
		t1.interrupt();
	}
	
	/**
	 * 测试TimeUnit 
	 */
	public void test1(){
			System.out.println( System.currentTimeMillis());
	
		
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println( System.currentTimeMillis());
	}
	
	/**
	 * 测试break 的一个方法
	 */
	public void test2(){
			
		int  index=1;
		
		while(true){
			
			index++;
			
			if(index==5){
				System.out.println(index);
				break;
			}else{
				System.out.println(index+"喔喔");
			}
			
		}
		
		index++;
		System.out.println("最后的数据"+index);
		
		
	}
}
